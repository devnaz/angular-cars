import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CarService } from './car.service';
import { Car } from './car';

@Component({
    selector: 'car-list',
    template: `
        <nav>
            <a routerLink="/add">Add new car</a>
        </nav>

        <div *ngFor="let car of cars" class="item-car">
            <span>{{ car.name }}</span>
            <span>{{ car.color }}</span>
            <span>{{ car.release }} року</span>

            <button (click)="onEdit(car)">Edit </button>
            <button (click)="onDelete(car)">Delete</button>
        </div>

    `,
    styles: [
        `
            .item-car {
                margin-top: 10px;
                width: 500px;
            }
            .item-car > span {
                width: 90px;
                display: inline-block;
            }
        `
    ],
    providers: [ CarService ]
})
export class CarListComponent implements OnInit {
    cars: Car[];

    constructor(private _carService: CarService, private _router: Router) {}

    ngOnInit() {
        this._carService.getCars().then(cars => this.cars = cars);
    }

    onEdit(car: Car) {
        this._router.navigate(['edit', car.id]);
        // console.log(car)
    }

    onDelete(car: Car) {
        let index = this.cars.indexOf(car);
        if( index > -1 )
            this.cars.splice(index, 1);
    }
}
