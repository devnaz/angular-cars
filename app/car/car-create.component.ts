import { Component } from '@angular/core';
import { OnInit } from '@angular/core';

import { Location } from '@angular/common';

// import { CarService } from './car.service';
import { Car } from './car';
import { CARS } from './data';

@Component({
    moduleId: module.id,
    selector: 'car-create',
    templateUrl: './car-create.component.html'
})
export class CarCreateComponent {
    cars = CARS;
    error: string = '';

    constructor(private _location: Location) {}

    onSave(name: string, color: string, release: string) {
        // console.log(name, color, release);
        if(name && color && release && release.length == 4) {
            this.cars.push(
                {
                    id: this.cars.length + 1,
                    name: name.trim(),
                    color: color.trim(),
                    release: parseInt(release)
                }
            )
            this.error = '';
            this.goBack();
        }
        else {
            this.error = 'Введіть правильно дані!'
        }
    }

    goBack() {
        this._location.back();
    }
}
