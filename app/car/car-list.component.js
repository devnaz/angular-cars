"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var car_service_1 = require('./car.service');
var CarListComponent = (function () {
    function CarListComponent(_carService, _router) {
        this._carService = _carService;
        this._router = _router;
    }
    CarListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this._carService.getCars().then(function (cars) { return _this.cars = cars; });
    };
    CarListComponent.prototype.onEdit = function (car) {
        this._router.navigate(['edit', car.id]);
        // console.log(car)
    };
    CarListComponent.prototype.onDelete = function (car) {
        var index = this.cars.indexOf(car);
        if (index > -1)
            this.cars.splice(index, 1);
    };
    CarListComponent = __decorate([
        core_1.Component({
            selector: 'car-list',
            template: "\n        <nav>\n            <a routerLink=\"/add\">Add new car</a>\n        </nav>\n\n        <div *ngFor=\"let car of cars\" class=\"item-car\">\n            <span>{{ car.name }}</span>\n            <span>{{ car.color }}</span>\n            <span>{{ car.release }} \u0440\u043E\u043A\u0443</span>\n\n            <button (click)=\"onEdit(car)\">Edit </button>\n            <button (click)=\"onDelete(car)\">Delete</button>\n        </div>\n\n    ",
            styles: [
                "\n            .item-car {\n                margin-top: 10px;\n                width: 500px;\n            }\n            .item-car > span {\n                width: 90px;\n                display: inline-block;\n            }\n        "
            ],
            providers: [car_service_1.CarService]
        }), 
        __metadata('design:paramtypes', [car_service_1.CarService, router_1.Router])
    ], CarListComponent);
    return CarListComponent;
}());
exports.CarListComponent = CarListComponent;
//# sourceMappingURL=car-list.component.js.map