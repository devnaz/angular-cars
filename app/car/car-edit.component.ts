import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { Location } from '@angular/common';

import { Car } from './car';
import { CarService } from './car.service';

@Component({
    moduleId: module.id,
    selector: 'car-edit',
    templateUrl: './car-edit.component.html',
    providers: [CarService]
})
export class CarEditComponent implements OnInit {
    car: Car;

    constructor(
        // private _location: Location,
        private _router: Router,
        private _activateRoute: ActivatedRoute,
        private _carService: CarService) {}

    ngOnInit() {
        this._activateRoute.params.forEach((params: Params) => {
            let id = +params['id']
            this._carService.getCar(id)
            .then(result => this.car = result)
        })
    }

    onClose() {
        this._router.navigate(['list']);
    }

}
