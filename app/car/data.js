"use strict";
exports.CARS = [
    { id: 1, name: 'BMW', color: 'black', release: 1990 },
    { id: 2, name: 'Fabia', color: 'red', release: 2000 },
    { id: 3, name: 'Shkoda', color: 'orange', release: 1980 }
];
//# sourceMappingURL=data.js.map