import { Injectable } from '@angular/core';

import { CARS } from './data';
import { Car } from './car';

@Injectable()
export class CarService {

    getCars(): Promise<Car[]> {
        return Promise.resolve(CARS);
    }

    getCar(id: number): Promise<Car> {
        return Promise.resolve(CARS).then(car => car.find(car => car.id == id ));
    }
}
