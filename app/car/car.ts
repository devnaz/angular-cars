export class Car {
    public id: number;
    public name: string;
    public color: string;
    public release: number;
}
