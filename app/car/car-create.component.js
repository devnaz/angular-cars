"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var data_1 = require('./data');
var CarCreateComponent = (function () {
    function CarCreateComponent(_location) {
        this._location = _location;
        this.cars = data_1.CARS;
        this.error = '';
    }
    CarCreateComponent.prototype.onSave = function (name, color, release) {
        // console.log(name, color, release);
        if (name && color && release && release.length == 4) {
            this.cars.push({
                id: this.cars.length + 1,
                name: name.trim(),
                color: color.trim(),
                release: parseInt(release)
            });
            this.error = '';
            this.goBack();
        }
        else {
            this.error = 'Введіть правильно дані!';
        }
    };
    CarCreateComponent.prototype.goBack = function () {
        this._location.back();
    };
    CarCreateComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'car-create',
            templateUrl: './car-create.component.html'
        }), 
        __metadata('design:paramtypes', [common_1.Location])
    ], CarCreateComponent);
    return CarCreateComponent;
}());
exports.CarCreateComponent = CarCreateComponent;
//# sourceMappingURL=car-create.component.js.map