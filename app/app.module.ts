import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { AppComponent } from './app.component';
import { CarListComponent } from './car/car-list.component';
import { CarCreateComponent } from './car/car-create.component';
import { CarEditComponent } from './car/car-edit.component';

import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'list', pathMatch: 'full' },
    { path: 'list', component: CarListComponent },
    { path: 'add', component: CarCreateComponent },
    { path: 'edit/:id', component: CarEditComponent }
];

@NgModule({
    imports: [ BrowserModule, FormsModule, RouterModule.forRoot(routes) ],
    declarations: [ AppComponent, CarListComponent, CarCreateComponent, CarEditComponent ],
    bootstrap: [ AppComponent ]
})
export class AppModule {}
